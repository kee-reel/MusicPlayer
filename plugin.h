#pragma once

#include <QtCore>

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"
#include "../../Interfaces/Utility/i_music_player_track_data_extention.h"
#include "../../Interfaces/Utility/i_music_player_track_progress_data_extention.h"

#include "musicplayer.h"

class Plugin : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "MASS.Module.MusicPlayer" FILE "PluginMeta.json")
	Q_INTERFACES(IPlugin)

public:
	Plugin();
	~Plugin() override;

private:
	MusicPlayer* m_impl;
	ReferenceInstancePtr<IMusicPlayerTrackDataExtention> m_tracks;
	ReferenceInstancePtr<IMusicPlayerTrackProgressDataExtention> m_tracksProgress;

protected:
	void onReady() override;
};
