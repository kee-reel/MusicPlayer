#pragma once

#include <QtCore>
#include <QMediaPlayer>
//#include <QMediaPlaylist>

#include "../../Interfaces/Utility/imusicplayer.h"
#include "../../Interfaces/Utility/i_music_player_track_data_extention.h"
#include "../../Interfaces/Architecture/referenceinstance.h"

class MusicPlayer : public QObject, public IMusicPlayer
{
	Q_OBJECT
	Q_INTERFACES(IMusicPlayer)
	Q_PROPERTY(int trackIndex READ trackIndex WRITE setTrackIndex NOTIFY trackIndexChanged)

public:
	MusicPlayer(QObject* parent, ReferenceInstancePtr<IMusicPlayerTrackDataExtention> tracks);
	virtual ~MusicPlayer() override;

public:
	Q_INVOKABLE void play() override;
	Q_INVOKABLE void next() override;
	Q_INVOKABLE void prev() override;
	Q_INVOKABLE void addTracks(QString path) override;
	Q_INVOKABLE void removeTrack(int index) override;

public:
	void init();
	int trackIndex() override;
	void setTrackIndex(int trackIndex) override;

signals:
	void trackIndexChanged(int trackIndex);

private slots:
	void onTrackChanged(int trackIndex);
	void onPositionChanged(qint64 position);

private:
	inline bool isPlaying()
	{
        return m_player.playbackState() == QMediaPlayer::PlaybackState::PlayingState;
	}

private:
	QMediaPlayer m_player;
//	QMediaPlaylist m_playlist;
	ReferenceInstancePtr<IMusicPlayerTrackDataExtention> m_tracks;
};
