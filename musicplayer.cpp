#include "musicplayer.h"
#include <QMediaPlayer>

#include "../../Interfaces/Utility/i_music_player_track_progress_data_extention.h"

MusicPlayer::MusicPlayer(QObject* parent, ReferenceInstancePtr<IMusicPlayerTrackDataExtention> tracks) :
	QObject(parent),
	m_tracks(tracks)
{
//	m_player.setPlaylist(&m_playlist);
//	connect(&m_playlist, &QMediaPlaylist::currentIndexChanged, this, &MusicPlayer::onTrackChanged);
	connect(&m_player, &QMediaPlayer::positionChanged, this, &MusicPlayer::onPositionChanged);
}

MusicPlayer::~MusicPlayer()
{
}

void MusicPlayer::init()
{
	auto model = m_tracks->getModel();
	auto index = model->index(0, 0);
	while(index.isValid())
	{
		auto item = m_tracks->getModel()->getItem(index);
		auto path = QUrl::fromLocalFile(item[INTERFACE(IMusicPlayerTrackDataExtention)]["path"].toString());
//		m_playlist.addMedia(path);
		index = model->index(index.row() + 1, 0);
	}
//	m_playlist.setCurrentIndex(0);
}

int MusicPlayer::trackIndex()
{
//	return m_playlist.currentIndex();
}

void MusicPlayer::setTrackIndex(int trackIndex)
{
//	m_playlist.setCurrentIndex(trackIndex);
//	emit trackIndexChanged(m_playlist.currentIndex());
}

void MusicPlayer::onTrackChanged(int trackIndex)
{
	auto model = m_tracks->getModel();
	auto index = model->index(trackIndex, 0);
	if(index.isValid())
	{
		auto item = m_tracks->getModel()->getItem(index);
		auto progress = item[INTERFACE(IMusicPlayerTrackProgressDataExtention)]["progress"];
		auto duration = progress.isNull()? 0 : progress.toInt();
		m_player.setPosition(duration);
	}
	emit trackIndexChanged(trackIndex);
}

void MusicPlayer::onPositionChanged(qint64 position)
{
	auto model = m_tracks->getModel();
//	model->updateItem(model->index(m_playlist.currentIndex(), 0), {
//		{	INTERFACE(IMusicPlayerTrackProgressDataExtention), {
//				{"progress", position}
//			}
//		}
//	});
}

void MusicPlayer::play()
{
	if(isPlaying())
	{
		m_player.stop();
	}
	else
	{
		m_player.play();
	}
}

void MusicPlayer::next()
{
//	m_playlist.next();
}

void MusicPlayer::prev()
{
//	m_playlist.previous();
}

void MusicPlayer::addTracks(QString paths)
{
	auto model = m_tracks->getModel();
	for(auto& path : paths.split(','))
	{
//		m_playlist.addMedia(QUrl(path));
		model->appendItem({
			{	INTERFACE(IMusicPlayerTrackDataExtention), {
					{"name", "Recursion"},
					{"path", path.remove("file://")},
				}
			}
		});
	}
}

void MusicPlayer::removeTrack(int index)
{
	auto model = m_tracks->getModel();
	if(index < model->rowCount())
	{
		model->removeItem(model->index(index, 0));
	}
}
